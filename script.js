$(window).on('load', function () {
    $('.gallery__photos').masonry({
        itemSelector: '.gallery__photos-item',
        gutter: 20,
    });
});


$(".work__item").append('<div class="work__item-settings"><div class="work__buttons"><div class="work__buttons-item work__buttons-connect"><i class="fas fa-link"></i></div><div class="work__buttons-item work__buttons-add"><div class="work__buttons-square-item"></div></div></div><span class="designs-title">creative design</span><span class="designs-subtitle">web design</span></div>')


$(".services__tabs-list").on("click", event => {
    if(event.target.classList.contains("services__tab") && !event.target.classList.contains("services__tab--active")) {
        $(".services__tab--active").removeClass("services__tab--active")
        $(".services__content--active").removeClass("services__content--active")
        const tabId = event.target.getAttribute("data-tab")
        $(tabId).addClass("services__content--active") 
        event.target.classList.add("services__tab--active")
    }
}) 

function addItems(maxNum, item, addBtn, loadingBtn) {
    $(`.${loadingBtn}`).removeClass(`${loadingBtn}--active`)
    $(`.${addBtn}`).addClass(`${addBtn}--active`)
    $(`.${item}`).each(function() {   
        if ($(`.${item}--active`).length < maxNum) {      // тут я даю условие для показа фото, можно вставить любое кол-во фото и всё будет работать
            $(this).addClass(`${item}--active`) 
        }
        if (maxNum === $(`.${item}`).length) {
            $(`.${addBtn}`).removeClass(`${addBtn}--active`)
        }
    })
}

let workItemsActive = $(".work__item--active") // - изначальное кол-во активных элементов 
let maxItemNum = $(".work__item--active").length * 2      // число-ограничитель одонразового показа картинок
$(".work__button").on("click", () => {
    $(".work__loading-btn").addClass("work__loading-btn--active")
    $(".work__button").removeClass("work__button--active")
    setTimeout(() => {
        addItems(maxItemNum, "work__item", "work__button", "work__loading-btn")
        maxItemNum += workItemsActive.length 
        const filterId = $(".work__filter--active").attr("data-filter")
        workItemsActive = $(".work__item--active")  // - кол-во активных элементов после добавления порции новых картинок
        $(".work__item--active").each(function() {
            if ($(this).attr("id") != filterId) {
                $(this).removeClass("work__item--active") 
            }
            if (filterId == "all") {
                $(this).addClass("work__item--active") 
            }
        })
    }, 2000)
    // для большей ясности того что тут написано - я делаю так как с фильтрами на сайтах, при активном фильтре
    // идёт добавление элементов сугубо этого фильтра 
})
 


$(".work__filters").on("click", event => {
    if(event.target.classList.contains("work__filter") && !event.target.classList.contains("work__filter--active")) {
        $(".work__filter--active").removeClass("work__filter--active")
        event.target.classList.add("work__filter--active")
        const filterId = event.target.getAttribute("data-filter")
        workItemsActive.each(function() {  
            if($(this).attr("id") != filterId) {
              $(this).removeClass("work__item--active")
            }
            if ($(this).attr("id") == filterId) {
              $(this).addClass("work__item--active")
            }
            if (filterId == "all"){
              $(this).addClass("work__item--active")
            }
        })
    }
})

const users = $(".reviews__slider-item")
let currentUserId = 0
$(".reviews__slider-users").on("click", event => {
    if(event.target.classList.contains("reviews__slider-item")  || event.target.classList.contains("reviews__slider-img")) {
        users.removeClass("reviews__slider-item--active")
        event.target.closest("div").classList.add("reviews__slider-item--active")
        const userId = event.target.closest("div").getAttribute("data-user")
        function changeClass(items, className) {
            items.each(function() {
                if ($(this).attr("id") == userId) {
                    $(this).addClass(className)
                }
                if ($(this).attr("id") != userId) {
                    $(this).removeClass(className)
                }
            })
        }
        changeClass($(".reviews__users-item"), "reviews__users-item--active")
        changeClass($(".reviews__text-item"), "reviews__text-item--active")
        changeClass($(".reviews__names-item"), "reviews__names-item--active")
        changeClass($(".reviews__job-item"), "reviews__job-item--active")
        currentUserId = +userId
    }
})

function addContent() {
    function changeClass(items, activeClass) {
        items.removeClass(activeClass)
        items[currentUserId].classList.add(activeClass)
    }
    changeClass(users, "reviews__slider-item--active")
    changeClass($(".reviews__users-item"), "reviews__users-item--active")
    changeClass($(".reviews__text-item"), "reviews__text-item--active")
    changeClass($(".reviews__names-item"), "reviews__names-item--active")
    changeClass($(".reviews__job-item"), "reviews__job-item--active")
}

const showNextUser = function () {
    currentUserId++ 
    currentUserId >= users.length ? currentUserId = 0 : currentUserId
    addContent()
}
$(".reviews__btn-next").on("click", showNextUser)

const showPrevUser = function () {
    currentUserId--
    currentUserId < 0 ? currentUserId = 3 : currentUserId
    addContent()
}
$(".reviews__btn-prev").on("click", showPrevUser)



let galleryItemsActive = $(".gallery__photos-item--active") 
console.log(galleryItemsActive.length)
let galleryItemNum =  $(".gallery__photos-item--active").length * 2      
$(".gallery__button").on("click", () => {
    $(".gallery__loading-btn").addClass("gallery__loading-btn--active")
    $(".gallery__button").removeClass("gallery__button--active")
    setTimeout(() => {
        console.log(galleryItemNum)
        console.log($(".gallery__photos-item").length)
        addItems(galleryItemNum, "gallery__photos-item", "gallery__button", "gallery__loading-btn")
        $('.gallery__photos').masonry({
            itemSelector: '.gallery__photos-item',
            gutter: 20,
        });
        

        galleryItemNum += galleryItemsActive.length 
        galleryItemsActive = $(".gallery__photos-item--active") 
    }, 2000)
})
